package fr.rtone.demo.timezone;

import com.google.common.base.Strings;
import fr.rtone.demo.timezone.jdbc.JdbcDated;
import fr.rtone.demo.timezone.jdbc.JdbcDatedService;
import fr.rtone.demo.timezone.jpa.JpaDated;
import fr.rtone.demo.timezone.jpa.JpaDatedService;
import fr.rtone.demo.timezone.util.CustomException;
import fr.rtone.demo.timezone.util.DataBaseInitializer;
import org.jboss.weld.environment.se.bindings.Parameters;
import org.jboss.weld.environment.se.events.ContainerInitialized;
import org.joda.time.DateMidnight;
import org.joda.time.DateTimeZone;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.util.*;

import static java.lang.System.out;

@ApplicationScoped
public class App {

    private static final TimeZone GMT_TZ = TimeZone.getTimeZone("GMT");
    private static final String PRINT_TEMPLATE = "%-15s %-15s %-30s %-30s\n";

    @Inject @Parameters
    List<String> parameters;

    @Inject
    JdbcDatedService jdbcService;
    @Inject
    JpaDatedService jpaService;

    @Inject DataBaseInitializer dbInit;

    // Entry point with org.jboss.weld.environment.se.StartMain
    public void start(@Observes ContainerInitialized event) throws CustomException {
        dbInit.init();

        String javaTimezone = parameters.size() <= 1 ? GMT_TZ.getID() : parameters.get(1);
        TimeZone.setDefault(TimeZone.getTimeZone(javaTimezone));
        String dbTimezone = parameters.size() <= 2 ? "SYSTEM" : parameters.get(2);

        if (parameters.isEmpty() || parameters.get(0).toUpperCase().equals(Command.PULL.name())) {
            out.println("=== JDBC ===");
            printJdbc(jdbcService.pull(dbTimezone));
            out.println("=== JPA ===");
            printJpa(jpaService.pull(dbTimezone));
        } else {
            DateMidnight refDate = new DateMidnight(1986, 6, 19, DateTimeZone.forID(javaTimezone));

            out.println("=== JDBC ===");
            JdbcDated dated = new JdbcDated();
            dated.datetime = new Date(refDate.getMillis());
            dated.timestamp = new Date(refDate.getMillis());
            dated.javaTimezone = TimeZone.getDefault().getID();
            dated.dbTimezone = dbTimezone;
            print(jdbcService.push(dated));

            out.println("=== JPA ===");
            JpaDated jpaDated = new JpaDated();
            jpaDated.datetime = new Date(refDate.getMillis());
            jpaDated.timestamp = new Date(refDate.getMillis());
            jpaDated.javaTimezone = TimeZone.getDefault().getID();
            jpaDated.dbTimezone = dbTimezone;
            print(jpaService.push(jpaDated));

        }
    }

    private void printJdbc(Collection<JdbcDated> dateds) {
        printHeader();
        for (JdbcDated dated : dateds) {
            printLine(dated);
        }
        out.println();
    }
    private void print(JdbcDated dated) {
        printHeader();
        printLine(dated);
        out.println();
    }
    private void printLine(JdbcDated dated) {
        out.printf(PRINT_TEMPLATE, dated.javaTimezone, dated.dbTimezone, dated.timestamp, dated.datetime);
    }
    private void printJpa(Collection<JpaDated> dateds) {
        printHeader();
        for (JpaDated dated : dateds) {
            printLine(dated);
        }
        out.println();
    }
    private void print(JpaDated dated) {
        printHeader();
        printLine(dated);
        out.println();
    }
    private void printLine(JpaDated dated) {
        out.printf(PRINT_TEMPLATE, dated.javaTimezone, dated.dbTimezone, dated.timestamp, dated.datetime);
    }
    private void printHeader() {
        out.println();
        String header = String.format(PRINT_TEMPLATE, "javaTimezone", "dbTimezone", "timestamp", "datetime");
        out.println(Strings.repeat("-", header.length()));
        out.print(header);
        out.println(Strings.repeat("-", header.length()));
    }

    enum Command { PULL, PUSH }
}
