package fr.rtone.demo.timezone.jpa;

import fr.rtone.demo.timezone.util.DataBaseInitializer;
import fr.rtone.demo.timezone.util.ResourceProducer;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;

import static fr.rtone.demo.timezone.util.DataBaseInitializer.GMT_TZ;
import static org.fest.assertions.Assertions.assertThat;

@RunWith(Arquillian.class)
public class JpaDatedServiceTest {

    @Inject
    JpaDatedService service;
    @Inject
    DataBaseInitializer dbInitializer;

    @Deployment
    public static JavaArchive createTestArchive() {
        return ShrinkWrap.create(JavaArchive.class)
                .addPackage(JpaDatedService.class.getPackage())
                .addPackage(ResourceProducer.class.getPackage())
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsResource("META-INF/persistence.xml", "META-INF/persistence.xml");
    }

    @Test
    public void testPull() throws Exception {
        dbInitializer.populateForTest();

        String gmt = GMT_TZ.getID();
        List<JpaDated> dateds = service.pull(gmt);

        assertThat(dateds).isNotEmpty();
        assertThat(dateds.get(0).dbTimezone).isEqualTo("SYSTEM");
        assertThat(dateds.get(0).javaTimezone).isEqualTo(gmt);

    }

    @Test
    public void testPush() throws Exception {
        String gmt = GMT_TZ.getID();
        Date sampleDate = new Date(1000000000);

        JpaDated gmtDated = new JpaDated();
        gmtDated.datetime = sampleDate;
        gmtDated.timestamp = sampleDate;
        gmtDated.dbTimezone = gmt;
        gmtDated.javaTimezone = "Europe/Paris";

        JpaDated updatedDated = service.push(gmtDated);

        assertThat(updatedDated.datetime).isEqualTo(updatedDated.timestamp);
        assertThat(updatedDated.dbTimezone).isEqualTo("N/A"); // H2 does note support session TZ
    }
}

