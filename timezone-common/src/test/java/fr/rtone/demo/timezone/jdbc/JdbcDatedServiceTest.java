package fr.rtone.demo.timezone.jdbc;

import fr.rtone.demo.timezone.util.DataBaseInitializer;
import fr.rtone.demo.timezone.util.ResourceProducer;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;

import static fr.rtone.demo.timezone.util.DataBaseInitializer.GMT_TZ;
import static org.fest.assertions.Assertions.*;

@RunWith(Arquillian.class)
public class JdbcDatedServiceTest {
    @Inject
    JdbcDatedService service;
    @Inject
    DataBaseInitializer dbInitializer;

    @Deployment
    public static JavaArchive createTestArchive() {
        return ShrinkWrap.create(JavaArchive.class)
                         .addPackage(JdbcDatedService.class.getPackage())
                         .addPackage(ResourceProducer.class.getPackage())
                         .addAsResource("jdbc.properties")
                         .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Before
    public void setupDB() throws Exception {
        dbInitializer.init();
        dbInitializer.populateForTest();
    }

    @Test
    public void testPull() throws Exception {
        String gmt = GMT_TZ.getID();
        List<JdbcDated> dateds = service.pull(gmt);

        assertThat(dateds).isNotEmpty();
        assertThat(dateds.get(0).dbTimezone).isEqualTo("SYSTEM");
        assertThat(dateds.get(0).javaTimezone).isEqualTo(gmt);
    }

    @Test
    public void testPush() throws Exception {
        String gmt = GMT_TZ.getID();
        Date sampleDate = new Date(1000000000);

        JdbcDated gmtDated = new JdbcDated();
        gmtDated.datetime = sampleDate;
        gmtDated.timestamp = sampleDate;
        gmtDated.dbTimezone = gmt;
        gmtDated.javaTimezone = "Europe/Paris";

        JdbcDated updatedDated = service.push(gmtDated);

        assertThat(updatedDated.datetime).isEqualTo(updatedDated.timestamp);
        assertThat(updatedDated.dbTimezone).isEqualTo("N/A"); // H2 does note support session TZ
    }

}
