CREATE DATABASE tz;
CREATE USER tz IDENTIFIED BY 'tzpwd';
USE tz
GRANT ALL PRIVILEGES ON tz.* TO tz;

# For changing session timezone
# $MYSQL_HOME/bin/mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql -u root mysql