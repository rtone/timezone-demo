package fr.rtone.demo.timezone.jpa;

import fr.rtone.demo.timezone.util.CustomException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.*;
import java.util.List;

public class JpaDatedService {

    private static final Logger logger = LoggerFactory.getLogger(JpaDatedService.class);

    @Inject
    private EntityManager entityManager;

    public List<JpaDated> pull(String dbTimezone) {
        updateSessionTimezone(dbTimezone, entityManager);

        return entityManager.createQuery("FROM JpaDated").getResultList();
    }

    public JpaDated push(JpaDated dated) {
        updateSessionTimezone(dated.dbTimezone, entityManager);
        dated.dbTimezone = findSessionTimezone(entityManager);

        try{
            Query query = entityManager.createQuery("FROM JpaDated WHERE dbTimezone = ? AND javaTimezone = ?");
            query.setParameter(1, dated.dbTimezone);
            query.setParameter(2, dated.javaTimezone);
            JpaDated result = (JpaDated) query.getSingleResult();

            result.timestamp = dated.timestamp;
            result.datetime = dated.datetime;

            return result;
        } catch (NoResultException e) {

            entityManager.persist(dated);
            return dated;
        }
    }

    private String findSessionTimezone(EntityManager entityManager) {
        if (isDatabaseMysql(entityManager)) {
            Query query = entityManager.createNativeQuery("SELECT @@session.time_zone");
            return (String) query.getSingleResult();
        } else {
            return "N/A";
        }
    }

    private void updateSessionTimezone(String tz, EntityManager entityManager) {
        if (isDatabaseMysql(entityManager)) {
            Query query = entityManager.createNativeQuery("SET time_zone = ?");
            query.setParameter(1, tz);
            if (query.executeUpdate() == 0) {
                logger.info("Nothing changed on session timezone");
            }
        }
    }

    private boolean isDatabaseMysql(EntityManager entityManager) {
        String dialect = (String) entityManager.getEntityManagerFactory().getProperties().get("hibernate.dialect");
        return org.hibernate.dialect.MySQL5InnoDBDialect.class.getName().equals(dialect);
    }
}
