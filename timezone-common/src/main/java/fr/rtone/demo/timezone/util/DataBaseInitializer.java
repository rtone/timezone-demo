package fr.rtone.demo.timezone.util;

import fr.rtone.demo.timezone.jdbc.JdbcDatedService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.TimeZone;

@ApplicationScoped
public class DataBaseInitializer {

    private static final Logger logger = LoggerFactory.getLogger(DataBaseInitializer.class);

    public static final TimeZone GMT_TZ = TimeZone.getTimeZone("GMT");

    @Inject
    private DataSource dataSource;

    public void init() {
        try (Connection connection = dataSource.getConnection()) {
            connection
                    .prepareStatement("CREATE TABLE Dated (" +
                            "id INTEGER AUTO_INCREMENT PRIMARY KEY, " +
                            "timestamp TIMESTAMP, " +
                            "datetime DATETIME, " +
                            "dbTimezone VARCHAR(30) NOT NULL," +
                            "javaTimezone VARCHAR(30) NOT NULL)")
                    .executeUpdate();
        } catch (SQLException e) {
            logger.info("Pas de création de la table 'Dated' : " + e.getMessage());
        }
    }

    public void populateForTest() throws CustomException{
        populateForTest("Dated");
        populateForTest("JpaDated");
    }

    public void populateForTest(String tableName) {
        try {
            PreparedStatement statement = dataSource.getConnection()
                    .prepareStatement("INSERT INTO " + tableName + " (timestamp, datetime, dbTimezone, javaTimezone) VALUES (?, ?, ?, ?)");
            statement.setTimestamp(1, new Timestamp(0));
            statement.setTimestamp(2, new Timestamp(0));
            statement.setString(3, "SYSTEM");
            statement.setString(4, "GMT");
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.info("Pas d'insertion dans la table '" + tableName + "' : " + e.getMessage());
        }
    }

}
