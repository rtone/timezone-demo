package fr.rtone.demo.timezone.jpa;

import com.google.common.base.Objects;

import javax.persistence.*;
import java.util.Date;

import static com.google.common.base.Objects.ToStringHelper;

@Entity
public class JpaDated {

    @Id @GeneratedValue
    public Long id;
    public String dbTimezone;
    public String javaTimezone;
    @Temporal(TemporalType.TIMESTAMP) @Column(columnDefinition = "timestamp")
    public Date timestamp;
    @Temporal(TemporalType.TIMESTAMP) @Column(columnDefinition = "datetime")
    public Date datetime;

    public JpaDated() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JpaDated dated = (JpaDated) o;

        return Objects.equal(this.dbTimezone, dated.dbTimezone) && Objects.equal(this.javaTimezone, dated.javaTimezone);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(dbTimezone, javaTimezone);
    }

    @Override
    public String toString() {
        ToStringHelper helper = Objects.toStringHelper(this);
        helper.add("dbTimezone", dbTimezone);
        helper.add("javaTimezone", javaTimezone);
        helper.add("timestamp", timestamp);
        helper.add("datetime", datetime);
        return helper.toString();
    }
}
