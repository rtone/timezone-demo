package fr.rtone.demo.timezone.util;

import com.jolbox.bonecp.BoneCPDataSource;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

public class ResourceProducer {

    private static final EntityManagerFactory EMF = Persistence.createEntityManagerFactory("default");

    @Produces @ApplicationScoped
    public DataSource buildDataSource() throws IOException {
        Properties properties = new Properties();

        URL resource = ResourceProducer.class.getClassLoader().getResource("jdbc.properties");
        try (InputStream stream = resource.openStream()){
            properties.load(stream);

            BoneCPDataSource ds = new BoneCPDataSource();
            ds.setJdbcUrl(properties.getProperty("jdbc.url"));
            ds.setUsername(properties.getProperty("jdbc.username"));
            ds.setPassword(properties.getProperty("jdbc.password"));

            return ds;
        }
    }

    @Produces
    public EntityManager produceEntityManager() {
        EntityManager entityManager = EMF.createEntityManager();
        entityManager.getTransaction().begin();
        return entityManager;
    }

    public void closeEntityManager(@Disposes EntityManager entityManager) {
        entityManager.getTransaction().commit();
        entityManager.close();
    }


}
