package fr.rtone.demo.timezone.jdbc;

import com.google.common.base.Objects;
import fr.rtone.demo.timezone.jpa.JpaDated;

import java.util.Date;

public class JdbcDated {
    public String dbTimezone;
    public String javaTimezone;
    public Date timestamp;
    public Date datetime;

    public JdbcDated() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JpaDated dated = (JpaDated) o;

        return Objects.equal(this.dbTimezone, dated.dbTimezone) && Objects.equal(this.javaTimezone, dated.javaTimezone);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(dbTimezone, javaTimezone);
    }

    @Override
    public String toString() {
        Objects.ToStringHelper helper = Objects.toStringHelper(this);
        helper.add("dbTimezone", dbTimezone);
        helper.add("javaTimezone", javaTimezone);
        helper.add("timestamp", timestamp);
        helper.add("datetime", datetime);
        return helper.toString();
    }
}
