package fr.rtone.demo.timezone.jdbc;

import com.jolbox.bonecp.ConnectionHandle;
import fr.rtone.demo.timezone.util.CustomException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class JdbcDatedService {

    private static final Logger logger = LoggerFactory.getLogger(JdbcDatedService.class);

    @Inject
    private DataSource dataSource;

    public List<JdbcDated> pull(String dbTimezone) throws CustomException {
        List<JdbcDated> result = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            updateSessionTimezone(dbTimezone, connection);

            PreparedStatement statement = connection.prepareStatement("SELECT dbTimezone, javaTimezone, timestamp, datetime FROM Dated");
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                JdbcDated dated = new JdbcDated();
                int index = 1;
                dated.dbTimezone = resultSet.getString(index++);
                dated.javaTimezone = resultSet.getString(index++);
                dated.timestamp = timeStampToDate(resultSet.getTimestamp(index++));
                dated.datetime = timeStampToDate(resultSet.getTimestamp(index++));
                result.add(dated);
            }

            return result;
        } catch (SQLException e) {
            throw new CustomException(e);
        }
    }

    public JdbcDated push(JdbcDated dated) throws CustomException {
        try (Connection connection = dataSource.getConnection()) {
            updateSessionTimezone(dated.dbTimezone, connection);
            // Check the update
            dated.dbTimezone = findSessionTimezone(connection);

            PreparedStatement updateStatement = connection.prepareStatement("UPDATE Dated SET dbTimezone = ?, javaTimezone = ?, timestamp = ?, datetime = ? WHERE dbTimezone = ? AND javaTimezone = ?");
            int index = 1;
            updateStatement.setString(index++, dated.dbTimezone);
            updateStatement.setString(index++, dated.javaTimezone);
            updateStatement.setTimestamp(index++, dateToTimeStamp(dated.timestamp));
            updateStatement.setTimestamp(index++, dateToTimeStamp(dated.datetime));
            updateStatement.setString(index++, dated.dbTimezone);
            updateStatement.setString(index++, dated.javaTimezone);
            int ok = updateStatement.executeUpdate();
            if (ok == 0) {
                PreparedStatement insertStatement = connection.prepareStatement("INSERT INTO Dated (dbTimezone, javaTimezone, timestamp, datetime) VALUES (?, ?, ?, ?)");
                index = 1;
                insertStatement.setString(index++, dated.dbTimezone);
                insertStatement.setString(index++, dated.javaTimezone);
                insertStatement.setTimestamp(index++, dateToTimeStamp(dated.timestamp));
                insertStatement.setTimestamp(index++, dateToTimeStamp(dated.datetime));
                ok = insertStatement.executeUpdate();
            }
            return ok == 0 ? null : dated;
        } catch (SQLException e) {
            throw new CustomException("Cannot push", e);
        }
    }

    private String findSessionTimezone(Connection connection) throws SQLException {
        if (isDatabaseMysql(connection)) {
            PreparedStatement tzStatement = connection.prepareStatement("SELECT @@session.time_zone");
            ResultSet resultSet = tzStatement.executeQuery();
            resultSet.next();
            return resultSet.getString(1);
        } else {
            return "N/A";
        }
    }

    private void updateSessionTimezone(String tz, Connection connection) throws SQLException {
        if (isDatabaseMysql(connection)) {
            PreparedStatement updateTzStatement = connection.prepareStatement("SET time_zone = ?");
            updateTzStatement.setString(1, tz);
            if (updateTzStatement.executeUpdate() == 0) {
                logger.info("Nothing changed on session timezone");
            }
        }
    }

    private boolean isDatabaseMysql(Connection connection) {
        if (connection instanceof ConnectionHandle) {
            return ((ConnectionHandle)connection).getInternalConnection().getClass().getName().contains("mysql");
        } else {
            return connection.getClass().getName().contains("mysql");
        }
    }

    private Timestamp dateToTimeStamp(Date date) {
        return new Timestamp(date.getTime());
    }

    private Date timeStampToDate(Timestamp timestamp) {
        return timestamp == null ? null : new Date(timestamp.getTime());
    }
}
